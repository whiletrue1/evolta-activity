<?php

declare(strict_types=1);

namespace App\Method;

use App\Entity\Telemetry;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Constraints\Required;
use Yoanm\JsonRpcParamsSymfonyValidator\Domain\MethodWithValidatedParamsInterface;
use Yoanm\JsonRpcServer\Domain\JsonRpcMethodInterface;

class PutTelemetryMethod implements JsonRpcMethodInterface, MethodWithValidatedParamsInterface
{
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function apply(array $paramList = null)
    {
        $entry = new Telemetry();
        $entry->setUrl($paramList['url']);
        $entry->setTs($paramList['ts']);
        $this->entityManager->persist($entry);
        $this->entityManager->flush();
        return 'ok';
    }

    public function getParamsConstraint(): Constraint
    {
        return new Collection(['fields' => [
            'url' => new Required([
                new Url()
            ]),
            'ts' => new Required([
                new Positive()
            ]),
        ]]);
    }
}
